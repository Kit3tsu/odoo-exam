# -*- coding: utf-8 -*-
{
    'name': "presence",

    'summary': """
        Module presence examen odoo""",

    'description': """
        odoo 
    """,

    'author': "Ricardo Jhon Botl",
    

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Presence',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],
    'application': 'True',

    # always loaded
    'data': [
        'views/views.xml',
        'views/templates.xml',
        'views/examen_view.xml',
        'views/menu_view.xml',
        'views/student_view.xml',
        'reports/presence_report.xml',
        'security/user_group.xml',
        'security/ir.model.access.csv',
        

    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}