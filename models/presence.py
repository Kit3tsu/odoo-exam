from odoo import api, fields, models


class Presence(models.Model):
    _name = 'presence.presence'
    _description = 'Lien entre un examen et un etudiant inscrit pour l\'examen'

    name = fields.Char(string='Name')
    etudiant_id = fields.Many2one(comodel_name='res.partner', string='Etudiant', domain="[('is_student','!=',False)]")
    examen_id = fields.Many2one(comodel_name='presence.examen', string='Examen' )
    confirmed = fields.Boolean(string ='Est present ?')
    
    
