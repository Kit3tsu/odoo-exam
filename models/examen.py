from odoo import api, fields, models


class Examen(models.Model):
    _name = 'presence.examen'
    _description = 'un Examen'

    name = fields.Char(string='Name')
    subject = fields.Char(string='Name')
    date = fields.Date(string='Date')
    duree = fields.Integer(string='Durée')
    status = fields.Selection(selection=[('b', 'Brouillon'), ('t', 'Terminé')])
    nb_etu = fields.Integer(string='Nombre d\'etudiant',compute="countEtu")
    presence_ids = fields.One2many(comodel_name='presence.presence', inverse_name='examen_id', string='Presence', compute ="presents")
    etudiant_ids = fields.Many2many('res.partner', store=False)
    
    
 
    
    @api.depends('presence_ids')
    def countEtu(self):
        for examen in self:
            examen.nb_etu = len(examen.presence_ids.filtered("confirmed"))

    @api.onchange('etudiant_ids')
    def presents(self):
        no_presence_etudiants = self.etudiant_ids - self.mapped('presence_ids.etudiant_id')
        for etudiant in no_presence_etudiants:
            self.env['presence.presence'].new({'etudiant_id': etudiant.id,'examen_id': self.id})

    def mark_valide(self):
        self.write({'status':'t'})

    
    