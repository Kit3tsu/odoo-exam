from odoo import api, fields, models


class Etudiant(models.Model):
    
    _inherit = 'res.partner'

    #name = fields.Char(string='Name')
    matricule = fields.Char(string='Matricule')
    presence_ids = fields.One2many(inverse_name = 'etudiant_id', comodel_name='presence.presence', string='Presence')
    is_student = fields.Boolean(default=False)

    
    
